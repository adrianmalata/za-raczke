package integration;

import client.Client;
import database.Database;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import utils.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

/**
 * ---------------------------------------------------------------------------------------------------------
 * First please launch StartServerTestingMode.java and after running the tests please turn it off manually
 * ---------------------------------------------------------------------------------------------------------
 **/

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class ServerClientTest extends Client {
    private static final Properties PROPERTIES = new PropertiesUtils().loadPropertiesFromFile();
    private static final String IP_ADDRESS = PROPERTIES.getProperty("CLIENT_IP");
    private static final int PORT = Integer.parseInt(PROPERTIES.getProperty("PORT"));
    private final Database DATABASE = new Database();
    private final TakeDataFromClient takeDataFromClient = new TakeDataFromClient(communication, DATABASE);

    @BeforeEach
    void setUpEnvironment() {
        connectToServer(IP_ADDRESS, PORT);
        sendAndReceiveMessage("wipe-out");
    }

    @AfterEach
    void resetClient() {
        stopClient();
    }

    /** CLIENT MESSAGES IN MAIN SCREEN SETTINGS TESTS **/

    @Test
    public void checkHelpMessage() {
        checkMessageHelper("help", new Messages().getHelpMessageJson());
    }

    @Test
    public void checkInfoMessage() {
        checkMessageHelper("info", new Messages().getInfoMessageJson());
    }

    @Test
    public void checkSignupProcedure() {
        addUserToUsersAtSignup("Adrian", "password", "admin");
        List<User> users = reloadUserDataFile();
        assertEquals("Adrian", users.get(0).login());
    }

    @Test
    public void checkLoginProcedure() {
        addUserToUsersAtSignup("Adrian", "password", "admin");
        loginUser("Adrian", "password");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("6");
        sendAndReceiveMessage("stop");
        assertTrue(response.toString().contains("Login: Successfully logged."));
    }

    /** MAILBOX READ UNREAD SENT FOLDERS SETTINGS TESTS **/

    @Test
    public void checkCannotRequestForThatPositionInFolder() {
        addUserToUsersAtSignup("Adrian", "password", "admin");
        loginUser("Adrian", "password");
        sendAndReceiveMessage("1");
        sendAndReceiveMessage("2");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("b");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("You cannot request for that position"));
    }

    @Test
    public void checkUnreadFolderOverload() {
        addAdminAndUserAtSignup();
        goToWriteScreen("Adrian", "password");
        for (int i=0; i < 5; i++) {
            sendProperMail();
            sendAndReceiveMessage("3");
        }
        sendAndReceiveMessage("1");
        sendAndReceiveMessage("Adam");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("5");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("You cannot send any mail to this user, because his UNREAD folder is full"));
    }

    @Test
    public void checkProperDisplayingMail() {
        addAdminAndUserAtSignup();
        goToWriteScreen("Adrian", "password");
        sendProperMail();
        sendAndReceiveMessage("6");
        loginUser("Adam", "another");
        sendAndReceiveMessage("1");
        sendAndReceiveMessage("1");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("b");
        sendAndReceiveMessage("b");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("Example body"));
    }

    /** MAILBOX WRITE FOLDER SETTINGS TESTS **/

    @Test
    public void checkCannotSendMailWithNoReceiver() {
        addUserToUsersAtSignup("Adrian", "password", "admin");
        goToWriteScreen("Adrian", "password");
        sendAndReceiveMessage("4");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("5");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("No receiver, cannot send mail"));
    }

    @Test
    public void checkCannotSendMailToInvalidUser() {
        addUserToUsersAtSignup("Adrian", "password", "admin");
        goToWriteScreen("Adrian", "password");
        sendAndReceiveMessage("1");
        sendAndReceiveMessage("User");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("5");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("There is no such user, please try again with proper login"));
    }

    @Test
    public void checkCannotSendMailWithoutSubject() {
        addAdminAndUserAtSignup();
        goToWriteScreen("Adrian", "password");
        sendAndReceiveMessage("1");
        sendAndReceiveMessage("Adam");
        sendAndReceiveMessage("4");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("5");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("No subject, cannot send mail"));
    }

    @Test
    public void checkCannotSendMailWithoutBody() {
        addAdminAndUserAtSignup();
        goToWriteScreen("Adrian", "password");
        sendAndReceiveMessage("1");
        sendAndReceiveMessage("Adam");
        sendAndReceiveMessage("2");
        sendAndReceiveMessage("Example subject");
        sendAndReceiveMessage("4");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("5");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("No text, cannot send mail"));
    }

    @Test
    public void checkCannotSendTooBigMail() {
        addAdminAndUserAtSignup();
        goToWriteScreen("Adam", "another");
        sendAndReceiveMessage("1");
        sendAndReceiveMessage("Adam");
        sendAndReceiveMessage("2");
        sendAndReceiveMessage("Example subject");
        sendAndReceiveMessage("3");
        sendAndReceiveMessage("Example body, example body, example body, example body, example body, example body, example body, example body, example body, example body, example body, example body, example body, example body, example body, example body, example body, example body, example");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("4");
        sendAndReceiveMessage("5");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("Mail is longer than 255 chars, you need to respect this size of mail"));
    }

    @Test
    public void checkSendingMail() {
        addAdminAndUserAtSignup();
        goToWriteScreen("Adrian", "password");
        sendProperMail();
        List<Mail> mails = reloadMailsDataFile();
        escapeLoginScreenAndStopClient();
        assertEquals(1, mails.size());
    }

    /** MAILBOX SETTINGS TESTS **/

    @ParameterizedTest
    @CsvSource({"Adrian, password, admin", "Adam, another, user"})
    public void checkMailboxSettingsLayout(String login, String password, String role) {
        addUserToUsersAtSignup(login, password, role);
        goToMailboxSettingsScreen(login, password);
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("7");
        sendAndReceiveMessage("5");
        sendAndReceiveMessage("5");
        escapeLoginScreenAndStopClient();
        if (role.equals("admin")) {
            assertTrue(response.toString().contains("This is Mailbox Settings screen as ADMIN, please make your choice"));
        } else {
            assertTrue(response.toString().contains("This is Mailbox Settings screen as USER, please make your choice"));
        }
    }

    @RepeatedTest(4)
    public void checkChangeLoginAndPassword(RepetitionInfo repetitionInfo) {
        String[][] data = {{"Adrian", "password", "admin"}, {"Adam", "another", "user"}};
        int repetition = repetitionInfo.getCurrentRepetition();
        String login = data[repetition % 2][0];
        String password = data[repetition % 2][1];
        String role = data[repetition % 2][2];
        if (repetition % 2 == 0) {
            checkChangeLoginAndPasswordHelper(login, password, role, repetition == 2 ? "1" : "2", repetition == 2 ? login.toUpperCase() : password.toUpperCase());
        } else {
            checkChangeLoginAndPasswordHelper(login, password, role, repetition == 1 ? "1" : "2", repetition == 1 ? login.toUpperCase() : password.toUpperCase());
        }
    }

    @Test
    public void checkGetAllUsersNames() {
        addAdminAndUserAtSignup();
        goToMailboxSettingsScreen("Adrian", "password");
        sendAndReceiveMessage("3");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("5");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("Adrian") && response.toString().contains("Adam"));
    }

    @RepeatedTest(2)
    public void checkDeleteAccount(RepetitionInfo repetitionInfo) {
        String[][] data = {{"Adrian", "password", "admin"}, {"Adam", "another", "user"}};
        int repetition = repetitionInfo.getCurrentRepetition();
        String login = data[repetition % 2][0];
        String password = data[repetition % 2][1];
        String role = data[repetition % 2][2];
        checkDeleteAccountHelper(login, password, role, login);
    }

    @Test
    public void checkChangeUsersRole() {
        addAdminAndUserAtSignup();
        goToMailboxSettingsScreen("Adrian", "password");
        sendAndReceiveMessage("6");
        sendAndReceiveMessage("Adam");
        sendAndReceiveMessage("admin");
        sendAndReceiveMessage("5");
        sendAndReceiveMessageAsStringBuilder("stop");
        List<User> users = reloadUserDataFile();
        assertTrue(users.contains(new User("Adam", "another", UserRole.ADMIN)));
    }

    @Test
    public void checkAllUsersData() {
        addAdminAndUserAtSignup();
        goToMailboxSettingsScreen("Adrian", "password");
        sendAndReceiveMessage("7");
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("5");
        sendAndReceiveMessage("5");
        escapeLoginScreenAndStopClient();
        assertTrue(response.toString().contains("another"));
    }

    @Test
    public void checkAddNewUserAsAdmin() {
        addAdminAndUserAtSignup();
        goToMailboxSettingsScreen("Adrian", "password");
        sendAndReceiveMessage("8");
        sendAndReceiveMessageAsStringBuilder("Ola");
        sendAndReceiveMessage("otherPassword");
        sendAndReceiveMessage("user");
        sendAndReceiveMessage("5");
        sendAndReceiveMessage("stop");
        List<User> users = reloadUserDataFile();
        assertEquals(3, users.size());
    }

    /** SUPPORTING METHODS **/

    private void checkMessageHelper(String message, String expected) {
        sendAndReceiveMessage(message);
        StringBuilder response = sendAndReceiveMessageAsStringBuilder("stop");
        assertTrue(response.toString().contains(expected));
    }

    private void sendProperMail() {
        sendAndReceiveMessage("1");
        sendAndReceiveMessage("Adam");
        sendAndReceiveMessage("2");
        sendAndReceiveMessage("Example subject");
        sendAndReceiveMessage("3");
        sendAndReceiveMessage("Example body");
        sendAndReceiveMessage("4");
    }

    private void checkChangeLoginAndPasswordHelper(String login, String password, String role, String message, String changedValue) {
        addUserToUsersAtSignup(login, password, role);
        goToMailboxSettingsScreen(login, password);
        sendAndReceiveMessage(message);
        if (role.equals("admin")) {
            sendAndReceiveMessage(login);
        }
        sendAndReceiveMessage(changedValue);
        sendAndReceiveMessageAsStringBuilder("5");
        communication.sendMessage("stop");
        List<User> users = reloadUserDataFile();
        if (message.equals("1")) {
            assertEquals(changedValue, users.get(0).login());
        } else if (message.equals("2")) {
            assertEquals(changedValue, users.get(0).password());
        }
    }

    private void checkDeleteAccountHelper(String login, String password, String role, String userToDelete) {
        List<User> users = addUserToUsersAtSignup(login, password, role);
        assertTrue(users.contains(new User(login, password, UserRole.valueOf(role.toUpperCase()))));
        goToMailboxSettingsScreen(login, password);
        sendAndReceiveMessage("4");
        if (role.equals("admin")) {
            sendAndReceiveMessage(userToDelete);
            sendAndReceiveMessageAsStringBuilder("5");
        }
        communication.sendMessage("stop");
        while (!users.isEmpty()) {
            users = takeDataFromClient.loadTableToList("users", User.class);
        }
        assertFalse(users.contains(new User(login, password, UserRole.valueOf(role.toUpperCase()))));
    }

    private void addAdminAndUserAtSignup() {
        addUserToUsersAtSignup("Adrian", "password", "admin");
        addUserToUsersAtSignup("Adam", "another", "user");
    }

    private List<User> addUserToUsersAtSignup(String login, String password, String role) {
        signupUserAtTheBeginning(login, password, role);
        return reloadUserDataFile();
    }

    private List<User> reloadUserDataFile() {
        List<User> users = new ArrayList<>();
        while (users.isEmpty()) {
            users = takeDataFromClient.loadTableToList("users", User.class);
        }
        return users;
    }

    private List<Mail> reloadMailsDataFile() {
        List<Mail> mails = new ArrayList<>();
        while (mails.isEmpty()) {
            mails = takeDataFromClient.loadTableToList("mails", Mail.class);
        }
        return mails;
    }

    private void signupUserAtTheBeginning(String login, String password, String role) {
        sendAndReceiveMessage("signup");
        sendAndReceiveMessage(login);
        sendAndReceiveMessage(password);
        sendAndReceiveMessage(role);
    }

    private void loginUser(String login, String password) {
        sendAndReceiveMessage("login");
        sendAndReceiveMessage(login);
        sendAndReceiveMessage(password);
    }

    private void goToWriteScreen(String login, String password) {
        loginUser(login, password);
        sendAndReceiveMessage("3");
    }

    private void goToMailboxSettingsScreen(String login, String password) {
        loginUser(login, password);
        sendAndReceiveMessage("5");
    }

    private void escapeLoginScreenAndStopClient() {
        sendAndReceiveMessage("6");
        communication.sendMessage("stop");
    }

    private StringBuilder sendAndReceiveMessageAsStringBuilder(String messageToSend) {
        communication.sendMessage(messageToSend);
        return receiveMessages();
    }

    private void sendAndReceiveMessage(String messageToSend) {
        communication.sendMessage(messageToSend);
        receiveMessages();
    }
}