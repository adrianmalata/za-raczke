## Application-C-S

***

### Link

Stage 1- connection and 4 commands in JSON: https://youtu.be/OS-TztaVocM

Stage 2- mailbox in JSON: https://youtu.be/uXsd0fC93JY

Stage 3- tests: https://youtu.be/R7rjuyJ0MGE

Stage 4- DB connection: https://youtu.be/tbzlbHRsMTk

### Name

Simple mail client-server application.

### Description

This application allows you to launch a server and a client, establish a connection and:
* Get help message, that shows you possible commands (help, info, uptime, stop)
* Signup a new account
* Login into created account
* Send a message to another user
* Read messages from another users
* Change some data like your login, password
* Delete your account

There are two roles: user and admin: admin role gets you more settings available.

Application in the newest version is working with PostgreSQL database and has integration tests.

### Guide

1. Check the settings of socket connection (port, etc.)
2. Check the settings of database connection

Normal mode:
1. Run StartServerNormalMode.java
2. Run StartClient.java
3. When you want to finish, just follow the instructions and type “stop”.

Testing mode:
1. Run StartServerTestingMode.java
2. Run ServerClientTest.java
3. After testing shut manually StartServerTestingMode.java

### Libraries

* JUnit Jupiter 5.10.1 
* JDBC 42.6.0
* JOOQ 3.18.7

***