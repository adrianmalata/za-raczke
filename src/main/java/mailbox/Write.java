package mailbox;

import database.Database;
import utils.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Write extends MailboxFolder {

    private final Mail NEW_MAIL = new Mail(USER);

    public Write(IOCommunication communication, Database database, User user) {
        super(communication, database, user);
    }

    @Override
    public void start() {
        while (!exitFlag) {
            users = loadTableToList(USERS_TABLE, User.class);
            displayWriteWelcomeScreen();
            setChoice();
        }
    }

    private void displayWriteWelcomeScreen() {
        String message = getWriteWelcomeScreen();
        sendMessageWithIOCommunication(message);
    }

    protected void setChoice() {
        exitFlag = false;
        String choice = takeMessageWithIOCommunication();
        switch (choice) {
            case "1" -> setReceiver();
            case "2" -> setSubject();
            case "3" -> writeMail();
            case "4" -> sendMail();
            case "5" -> exitFlag = true;
            default -> sendMessageWithIOCommunication(getDidntRecognizeThisChoice());
        }
    }

    private void setReceiver() {
        sendMessageWithIOCommunication(getWriteReceiver());
        String receiverLogin = takeMessageWithIOCommunication();
        try {
            boolean unreadFull = setReceiverProperly(receiverLogin);
            if (NEW_MAIL.getReceiver() == null && !unreadFull) {
                sendMessageWithIOCommunication(getNoSuchUser());
            }
        } catch (NullPointerException ignored) {}
    }

    private boolean setReceiverProperly(String receiverLogin) {
        for (User user : users) {
            if (receiverLogin.equals(user.login())) {
                int unreadCounter = getNumberOfUsersUnreadMails(user, receiverLogin);
                if (unreadCounter > 5) {
                    sendMessageWithIOCommunication(getUnreadFolderIsFull());
                    return true;
                } else {
                    NEW_MAIL.setReceiver(receiverLogin);
                    break;
                }
            }
        }
        return false;
    }

    private int getNumberOfUsersUnreadMails(User user, String receiverLogin) {
        int unreadCounter = 1;
        for (Mail mail : mails) {
            if (receiverLogin.equals(user.login()) && mail.getMailStatus() == MailStatus.UNREAD) {
                unreadCounter++;
            }
        }
        return unreadCounter;
    }

    private void setSubject() {
        sendMessageWithIOCommunication(getWriteMailSubject());
        String subject = takeMessageWithIOCommunication();
        NEW_MAIL.setSubject(subject);
    }

    private void writeMail() {
        sendMessageWithIOCommunication(getWriteMailBody());
        String mail = takeMessageWithIOCommunication();
        if (mail.chars().count() > 255) {
            sendMessageWithIOCommunication(getMailIsLongerThan255Chars());
        } else {
            NEW_MAIL.setMail(mail);
        }
    }

    private void sendMail() {
        NEW_MAIL.setDate(LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)));
        if (NEW_MAIL.getReceiver() == null) {
            sendMessageWithIOCommunication(getNoElementCannotSendMail("receiver"));
        } else if (NEW_MAIL.getSubject() == null) {
            sendMessageWithIOCommunication(getNoElementCannotSendMail("subject"));
        } else if (NEW_MAIL.getMail() == null) {
            sendMessageWithIOCommunication(getNoElementCannotSendMail("text"));
        } else {
            mails.add(NEW_MAIL);
            saveAllDataToTable(mails, MAILS_TABLE, Mail.class);
            exitFlag = true;
        }
    }

    private String getWriteWelcomeScreen() {
        String receiverLogin = (NEW_MAIL.getReceiver() == null) ? "none" : NEW_MAIL.getReceiver();
        String subject = (NEW_MAIL.getSubject() == null) ? "empty" : NEW_MAIL.getSubject();
        return String.format(
                """
                ------------------------------------------------
                This is Write screen. Please fill up following information:
                1 - To: %s
                2 - Subject: %s
                3 - Body
                4 - Send mail
                5 - Go back to Mailbox
                ------------------------------------------------
                """, receiverLogin, subject);
    }

    private String getWriteReceiver() {
        return "Write receiver here:\n";
    }

    private String getWriteMailSubject() {
        return "Write mail subject here:\n";
    }

    private String getWriteMailBody() {
        return "Write mail here (max 255 chars):\n";
    }

    private String getNoSuchUser() {
        return "There is no such user, please try again with proper login";
    }

    private String getMailIsLongerThan255Chars() {
        return "Mail is longer than 255 chars, you need to respect this size of mail";
    }

    private String getNoElementCannotSendMail(String cause) {
        return String.format("No %s, cannot send mail", cause);
    }

    private String getUnreadFolderIsFull() {
        return "You cannot send any mail to this user, because his UNREAD folder is full";
    }
}