package mailbox;

import database.Database;
import utils.IOCommunication;
import utils.MailboxFolder;
import utils.User;

public class Sent extends MailboxFolder {

    public Sent(IOCommunication communication, Database database, User user) {
        super(communication, database, user);
    }

}
