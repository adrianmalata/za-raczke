package mailbox;

import database.Database;
import utils.IOCommunication;
import utils.User;

public class Mailbox {

    private final IOCommunication COMMUNICATION;
    private final User USER;
    protected final Database DATABASE;
    private boolean exitFlag;

    public Mailbox(IOCommunication communication, Database database, User user) {
        this.COMMUNICATION = communication;
        this.USER = user;
        this.DATABASE = database;
    }

    public void start() {
        while (!exitFlag) {
            displayWelcomeScreen();
            setChoice();
        }
    }

    private void displayWelcomeScreen() {
        sendMessageWithIOCommunication(getWelcomeScreen());
    }

    private void setChoice() {
        exitFlag = false;
        String choice = takeMessageWithIOCommunication();
        try {
            switch (choice) {
                case "1" -> new Unread(COMMUNICATION, DATABASE, USER).start();
                case "2" -> new Read(COMMUNICATION, DATABASE, USER).start();
                case "3" -> new Write(COMMUNICATION, DATABASE, USER).start();
                case "4" -> new Sent(COMMUNICATION, DATABASE, USER).start();
                case "5" -> exitFlag = new MailboxSettings(COMMUNICATION, DATABASE, USER).start(exitFlag);
                case "6" -> exitFlag = true;
                default -> sendMessageWithIOCommunication(getDidntRecognizeThisChoice());
            }
        } catch (NullPointerException ignored) {}
    }

    private void sendMessageWithIOCommunication(String message) {
        COMMUNICATION.sendMessage(message);
    }

    private String takeMessageWithIOCommunication() {
        return COMMUNICATION.takeMessage();
    }

    private String getWelcomeScreen() {
        return String.format(
                """
                ------------------------------------------------
                Welcome %s, please make your choice:
                1 - Show UNREAD mails
                2 - Show READ mails
                3 - WRITE a new mail
                4 - Show SENT mails
                5 - Go to Mailbox Settings
                6 - Log out
                ------------------------------------------------
                """, USER.login());
    }

    private String getDidntRecognizeThisChoice() {
        return "I didn't recognize this choice";
    }
}
