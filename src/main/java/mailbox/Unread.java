package mailbox;

import database.Database;
import utils.IOCommunication;
import utils.MailboxFolder;
import utils.User;

public class Unread extends MailboxFolder {

    public Unread(IOCommunication communication, Database database, User user) {
        super(communication, database, user);
    }

}
