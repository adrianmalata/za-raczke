package mailbox;

import database.Database;
import utils.*;

public class MailboxSettings extends TakeDataFromClient {

    private User USER;
    private boolean exitFlag;

    public MailboxSettings(IOCommunication communication, Database database, User user) {
        super(communication, database);
        this.USER = user;
    }

    public boolean start(boolean exitFlag) {
        if (USER.userRole().equals(UserRole.USER)) {
            exitFlag = showMailboxSettings(UserRole.USER);
        } else if (USER.userRole().equals(UserRole.ADMIN)) {
            exitFlag = showMailboxSettings(UserRole.ADMIN);
        } else {
            sendMessageWithIOCommunication(getInvalidUserRole());
        }
        return exitFlag;
    }

    private boolean showMailboxSettings(UserRole userRole) {
        while (!exitFlag) {
            users = loadTableToList(USERS_TABLE, User.class);
            displayWelcomeScreen(userRole);
            setChoice(userRole);
            saveAllDataToTable(users, USERS_TABLE, User.class);
        }
        return true;
    }

    private void displayWelcomeScreen(UserRole userRole) {
        String properWelcomeScreen = getProperWelcomeScreen(userRole);
        sendMessageWithIOCommunication(properWelcomeScreen);
    }

    private void setChoice(UserRole userRole) {
        exitFlag = false;
        String choice = takeMessageWithIOCommunication();
        switch (choice) {
            case "1" -> changeData(userRole, USER, "login");
            case "2" -> changeData(userRole, USER, "password");
            case "3" -> showAllUsersNames();
            case "4" -> deleteAccount(userRole, USER);
            case "5" -> exitFlag = true;
            case "6", "7", "8" -> {
                switch (choice) {
                    case "6" -> changeData(userRole, USER, "role");
                    case "7" -> showAllUsersLoginData();
                    case "8" -> addNewAccount();
                }
            }
            default -> sendMessageWithIOCommunication(getDidntRecognizeThisChoice());
        }
    }

    private void changeData(UserRole userRole, User user, String action) {
        if (userRole == UserRole.USER) {
            switch (action) {
                case "login" -> {
                    String newLogin = takeUsersLogin(true);
                    User newUser = new User(newLogin, user.password(), user.userRole());
                    users.set(users.indexOf(user), newUser);
                    this.USER = newUser;
                    changeMails(user, newLogin);
                }
                case "password" -> {
                    String newPassword = "";
                    newPassword = takePasswordFromClient(newPassword);
                    User newUser = new User(user.login(), newPassword, user.userRole());
                    users.set(users.indexOf(user), newUser);
                    this.USER = newUser;
                }
                case "role" -> {
                    UserRole newUserRole;
                    newUserRole = takeRoleFromClient();
                    users.set(users.indexOf(user), new User(user.login(), user.password(), newUserRole));
                }
            }
        } else if (userRole == UserRole.ADMIN) {
            String usersLogin = takeUsersLogin(false);
            User userToChange = getUserFromGivenLogin(usersLogin);
            changeData(UserRole.USER, userToChange, action);
        }
    }

    private void changeMails(User user, String newLogin) {
        for (Mail mail : mails) {
            if (mail.getSender().equals(user.login()) || mail.getReceiver().equals(user.login())) {
                String sender = mail.getSender().equals(user.login()) ? newLogin : mail.getSender();
                String receiver = mail.getReceiver().equals(user.login()) ? newLogin : mail.getReceiver();
                String date = mail.getDate();
                MailStatus mailStatus = mail.getMailStatus();
                String subject = mail.getSubject();
                String mailBody = mail.getMail();
                mails.set(mails.indexOf(mail), new Mail(sender, receiver, date, mailStatus, subject, mailBody));
            }
        }
        saveAllDataToTable(mails, MAILS_TABLE, Mail.class);
    }

    private void addNewAccount() {
        User newAccount = makeUserFromClientInput();
        users.add(newAccount);
    }

    private void deleteAccount(UserRole userRole, User user) {
        if (userRole == UserRole.USER) {
            users.remove(user);
            if (USER.userRole() == UserRole.USER) {
                exitFlag = true;
            }
        } else if (userRole == UserRole.ADMIN) {
            String usersLogin = takeUsersLogin(false);
            User userToDelete = getUserFromGivenLogin(usersLogin);
            deleteAccount(UserRole.USER, userToDelete);
        }
    }

    private void showAllUsersNames() {
        String message = getAllUsersNames();
        sendMessageWithIOCommunication(message);
    }

    private void showAllUsersLoginData() {
        String message = getAllUsersLoginData();
        sendMessageWithIOCommunication(message);
    }

    private String takeUsersLogin(boolean isNewLogin) {
        boolean[] setOfConditions = new boolean[2];
        if (isNewLogin) {
            setOfConditions[0] = true;
        } else {
            setOfConditions[1] = true;
        }
        return takeLoginFromClient("", setOfConditions);
    }

    private User getUserFromGivenLogin(String login) {
        User[] userToChange = new User[1];
        takeDataFromUserData(user -> userToChange[0] = new User(user.login(), user.password(), user.userRole()), login);
        return userToChange[0];
    }

    private String getInvalidUserRole() {
        return "MailboxSettings: Invalid userRole. Cannot start.";
    }

    private String getProperWelcomeScreen(UserRole userRole) {
        String message;
        if (userRole == UserRole.USER) {
            message = String.format(
                    """
                    ------------------------------------------------
                    This is Mailbox Settings screen as %s, please make your choice:
                    1 - Change your login
                    2 - Change your password
                    3 - Get all user names
                    4 - Delete your account
                    5 - Get back to Mailbox
                    ------------------------------------------------
                    """, USER.userRole());
        } else if (userRole == UserRole.ADMIN) {
            message = String.format(
                    """
                    ------------------------------------------------
                    This is Mailbox Settings screen as %s, please make your choice:
                    1 - Change user's login
                    2 - Change user's password
                    3 - Get all user names
                    4 - Delete user's account
                    5 - Get back to Mailbox
                    6 - Change user's userRole
                    7 - Get all users login data
                    8 - Add new user
                    ------------------------------------------------
                    """, USER.userRole());
        } else {
            message = String.format("%s: Invalid role", CLASS_NAME);
        }
        return message;
    }

    private String getAllUsersNames() {
        String list = "This is a list of all users names:";
        int i = 1;
        for (User user: users) {
            list = list
                    .concat(System.lineSeparator())
                    .concat(String.format("%d\t%s", i, user.login()));
            i++;
        }
        return list;
    }

    public String getAllUsersLoginData() {
        String list = "This is a list of all users with all information:";
        int i = 1;
        for (User user: users) {
            list = list.concat(System.lineSeparator())
                    .concat(String.format(
                            "%d\tLogin: %s, Password: %s, Role: %s",
                            i, user.login(), user.password(), user.userRole()));
            i++;
        }
        return list;
    }
}
