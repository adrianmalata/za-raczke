package mailbox;

import database.Database;
import utils.IOCommunication;
import utils.MailboxFolder;
import utils.User;

public class Read extends MailboxFolder {

    public Read(IOCommunication communication, Database database, User user) {
        super(communication, database, user);
    }

}
