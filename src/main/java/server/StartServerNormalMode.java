package server;

public class StartServerNormalMode extends StartServerBase {
    public static void main(String[] args) {
        Server server = initializeServer();
        server.runServer(PORT);
    }
}