package server;

public class StartServerTestingMode extends StartServerBase {

    public static void main(String[] args) {
        Server server = initializeServer();

        while (true) {
            server.runServer(PORT);
        }
    }
}
