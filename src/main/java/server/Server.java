package server;

import database.Database;
import login.Login;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import signup.Signup;
import utils.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Instant;

import static org.jooq.impl.SQLDataType.*;

public class Server {

    private final Instant SERVER_START_INSTANT = Instant.now();
    private final Messages MESSAGES = new Messages();
    private final String CLASS_NAME = getClass().getSimpleName();
    private final String USERS_TABLE = "users";
    private final String MAILS_TABLE = "mails";
    private final Database DATABASE;
    private final DSLContext JOOQ;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private IOCommunication communication;

    public Server(Database database) {
        this.DATABASE = database;
        this.JOOQ = DSL.using(DATABASE.getConnection(), SQLDialect.POSTGRES);
    }

    public void runServer(int port) {
        openServerSocket(port);
        startServer();
    }

    public void startServer() {
        connectClientSocket();
        openCommunication();
        System.out.printf(getConnectionEstablished());
        handleClientMessages();
    }

    private void openServerSocket(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException ioException) {
            System.err.printf("%s: I/O error occurred while opening the server socket\n", CLASS_NAME);
        } catch (IllegalArgumentException illegalArgumentException) {
            System.err.printf("%s: The port parameter is outside the specified range of valid port values, which is between 0 and 65535\n", CLASS_NAME);
        }
    }

    private void connectClientSocket() {
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException ioException) {
            System.err.printf("%s: I/O error occurred while connecting the client to the server\n", CLASS_NAME);
        }
    }

    private void openCommunication() {
        communication = new IOCommunication(clientSocket);
    }

    private void handleClientMessages() {
        String clientMessage;
        displayHomeScreen();
        while ((clientMessage = takeMessageWithIOCommunication()) != null) {
            System.out.printf("Server: Command %s\n", clientMessage);
            respondToClientMessage(clientMessage);
            displayHomeScreen();
        }
    }

    private void displayHomeScreen() {
        sendMessageWithIOCommunication(getHomeScreen());
    }

    private void respondToClientMessage(String clientMessage) {
        clientMessage = clientMessage.toLowerCase();
        switch (clientMessage) {
            case "signup" -> new Signup(communication, DATABASE).start();
            case "login" -> new Login(communication, DATABASE).start();
            case "help", "info", "uptime" -> sendMessageWithIOCommunication(getMessage(clientMessage));
            case "stop" -> stopServer();
            case "wipe-out" -> wipeOut();
            default -> sendMessageWithIOCommunication(getDefaultMessage());
        }
    }

    private String getMessage(String command) {
        String message;
        switch (command) {
            case "help" -> message = MESSAGES.getHelpMessageJson();
            case "info" -> message = MESSAGES.getInfoMessageJson();
            case "uptime" -> message = MESSAGES.getUptimeMessageJson(SERVER_START_INSTANT);
            default -> message = "";
        }
        return message;
    }

    private String getDefaultMessage() {
        return MESSAGES.getDefaultMessageString();
    }

    private void stopServer() {
        sendMessageWithIOCommunication(getClosingConnection());
        closeClientSocket();
        closeServerSocket();
        communication.closeCommunication();
        String className = new Throwable().getStackTrace()[1].getClassName();
        if (className.equals("StartServerNormalMode")) {
            DATABASE.closeConnection();
        }
    }

    private void closeClientSocket() {
        try {
            clientSocket.close();
        } catch (IOException ioException) {
            System.err.printf("%s: I/O error occurred while closing the client socket\n", CLASS_NAME);
        }
    }

    private void closeServerSocket() {
        try {
            serverSocket.close();
        } catch (IOException ioException) {
            System.err.printf("%s: I/O error occurred while closing the server socket\n", CLASS_NAME);
        }
    }

    private void wipeOut() {
        for (String table : new String[] {USERS_TABLE, MAILS_TABLE}) {
            JOOQ.dropTableIfExists(table).execute();
            System.out.printf("%s wiped-out\n", table);
            if (table.equals(USERS_TABLE)) {
                createEmptyUsersTable();
                System.out.printf("%s created\n", table);
            } else {
                createEmptyMailsTable();
                System.out.printf("%s created\n", table);
            }
        }
    }

    private void createEmptyUsersTable() {
        JOOQ.createTable(USERS_TABLE)
                .column("user_id", INTEGER.identity(true))
                .primaryKey("user_id")
                .column("login", VARCHAR(20))
                .column("password", VARCHAR(20))
                .column("user_role", VARCHAR(10).asConvertedDataType(new EnumConverter<>(UserRole.class)))
                .execute();
    }

    private void createEmptyMailsTable() {
        JOOQ.createTable(MAILS_TABLE)
                .column("mail_id", INTEGER.identity(true))
                .primaryKey("mail_id")
                .column("sender", VARCHAR(20))
                .column("receiver", VARCHAR(20))
                .column("date", VARCHAR(50))
                .column("mail_status", VARCHAR(10).asConvertedDataType(new EnumConverter<>(MailStatus.class)))
                .column("subject", VARCHAR(100))
                .column("mail", VARCHAR(255))
                .execute();
    }

    private void sendMessageWithIOCommunication(String message) {
        communication.sendMessage(message);
    }

    private String takeMessageWithIOCommunication() {
        return communication.takeMessage();
    }

    private String getConnectionEstablished() {
        return String.format("%s: Connection established\n", CLASS_NAME);
    }

    private String getHomeScreen() {
        return """
                -------------------------------------------------------------------------
                Welcome to the Post Office. Please choose what you want to do:
                Type "Signup" if you want to make a new account
                Type "Login" if you want to log into your existing account
                Type "Help" if you want to show the help message
                Type "Info" if you want to return server version number and creation date
                Type "Uptime" if you want to return the server lifetime
                Type "Stop" to shut down this program properly
                -------------------------------------------------------------------------
                """;
    }

    private String getClosingConnection() {
        return String.format("%s: Closing connection\n", CLASS_NAME);
    }
}
