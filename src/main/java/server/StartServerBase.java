package server;

import database.Database;
import utils.PropertiesUtils;

import java.util.Properties;

public class StartServerBase {
    protected static final Properties PROPERTIES = new PropertiesUtils().loadPropertiesFromFile();
    protected static final int PORT = Integer.parseInt(PROPERTIES.getProperty("PORT"));
    protected static final Database DATABASE = new Database();

    protected static Server initializeServer() {
        return new Server(DATABASE);
    }
}

