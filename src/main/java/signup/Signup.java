package signup;

import database.Database;
import utils.IOCommunication;
import utils.SignupAndLoginAbstractClass;

public class Signup extends SignupAndLoginAbstractClass {

    public Signup(IOCommunication communication, Database database) {
        super(communication, database);
    }

    @Override
    protected void takeInputFromClientInWelcomeScreen() {
        sendMessageWithIOCommunication(getWelcomeScreen());
        makeUserFromClientInput();
        acceptNewUserSignup();
    }

    protected void acceptNewUserSignup() {
        addNewUser();
        sendMessageWithIOCommunication(getThankYouForSignup());
        sendMessageWithIOCommunication(getClosingScreen());
    }
}