package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesUtils {

    public Properties loadPropertiesFromFile() {
        Properties properties = new Properties();
        try (InputStream inputStream = new FileInputStream("src/main/resources/config.properties")) {
            properties.load(inputStream);
        } catch (IOException ioException) {
            System.err.println("An error occurred while reading the configuration file.");
            throw new RuntimeException(ioException);
        } catch (NumberFormatException numberFormatException) {
            System.err.println("Invalid port number in the configuration file.");
            throw new RuntimeException(numberFormatException);
        } catch (NullPointerException nullPointerException) {
            System.err.println("inputStream parameter is null.");
        }
        return properties;
    }
}
