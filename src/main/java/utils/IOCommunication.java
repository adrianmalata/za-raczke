package utils;

import java.io.*;
import java.net.Socket;

public class IOCommunication {

    private PrintWriter output;
    private BufferedReader input;

    private final Socket SOCKET;

    public IOCommunication(Socket socket){
        this.SOCKET = socket;
        setOutput(socket);
        setInput(socket);
    }

    private void setOutput(Socket socket) {
        try {
            output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
        } catch (IOException ioException) {
            System.err.println("I/O error occurred while creating the output stream or if the socket wasn't connected");
        }
    }

    private void setInput(Socket socket) {
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException ioException) {
            System.err.println("I/O error occurred while creating the input stream or the socket was closed or the socket wasn't connected");
        }
    }

    public void sendMessage(String message) {
        output.println(message);
    }

    public String takeMessage() {
        try {
            return input.readLine();
        } catch (IOException ioException) {
            if (SOCKET.isClosed()) {
                System.out.println("Server: I/O communication with client is closed");
            } else {
                System.err.println("Server: I/O error occurred while reading command from client or communication with client is closed");
            }
        }
        return null;
    }

    public void closeCommunication() {
        try {
            output.close();
            input.close();
        } catch(IOException ioException) {
            System.err.println("Error occurred while closing the IOCommunication");
        }
    }
}
