package utils;

import org.jetbrains.annotations.NotNull;
import org.jooq.Converter;

public class EnumConverter<T extends Enum<T>> implements Converter<String, T> {

    private final Class<T> enumClass;

    public EnumConverter(Class<T> enumClass) {
        this.enumClass = enumClass;
    }

    @Override
    public T from(String databaseObject) {
        return databaseObject == null ? null : Enum.valueOf(enumClass, databaseObject);
    }

    @Override
    public String to(T userObject) {
        return userObject == null ? null : userObject.name();
    }

    @Override
    public @NotNull Class<String> fromType() {
        return String.class;
    }

    @Override
    public @NotNull Class<T> toType() {
        return enumClass;
    }
}
