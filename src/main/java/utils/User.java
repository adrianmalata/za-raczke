package utils;

import java.util.Objects;

public record User(String login, String password, UserRole userRole) {

    @Override
    public int hashCode() {
        return Objects.hash(login, password, userRole);
    }
}