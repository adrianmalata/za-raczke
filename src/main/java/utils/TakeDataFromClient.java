package utils;

import database.Database;
import org.jooq.*;
import org.jooq.Record;
import org.jooq.impl.DSL;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static org.jooq.impl.DSL.*;

public class TakeDataFromClient {
    protected final IOCommunication COMMUNICATION;
    protected final String USERS_TABLE = "users";
    protected final String MAILS_TABLE = "mails";
    protected final Database DATABASE;
    protected final DSLContext JOOQ;
    protected final String CLASS_NAME = getClass().getSimpleName();
    protected List<User> users;
    protected List<Mail> mails;
    protected User user;
    protected String login = "";
    protected String password = "";
    protected UserRole userRole;

    public TakeDataFromClient(IOCommunication communication, Database database) {
        this.COMMUNICATION = communication;
        this.DATABASE = database;
        this.JOOQ = DSL.using(DATABASE.getConnection(), SQLDialect.POSTGRES);
        this.users = loadTableToList(USERS_TABLE, User.class);
        this.mails = loadTableToList(MAILS_TABLE, Mail.class);
    }

    public <T> List<T> loadTableToList(String tableName, Class<T> valueClass) {
        if ("Login".equals(CLASS_NAME) && JOOQ.fetch(table(tableName)).isEmpty() && "users".equals(tableName)) {
            sendMessageWithIOCommunication(getDeclineReadingFile());
        }
        String idColumn = null;
        if (User.class.equals(valueClass)) {
            idColumn = "user_id";
        } else if (Mail.class.equals(valueClass)) {
            idColumn = "mail_id";
        }
        assert idColumn != null;
        return JOOQ.select().from(table(tableName)).orderBy(field(idColumn).asc()).fetch().map(new UserAndMailRecordMapper<>(valueClass));
    }

    private String getDeclineReadingFile() {
        return String.format("%s: File is empty. Please sign up to make an account.", CLASS_NAME);
    }

    public User makeUserFromClientInput() {
        String login = "";
        String password = "";
        UserRole userRole;
        login = takeLoginFromClient(login, new boolean [] {CLASS_NAME.equals("Signup"), CLASS_NAME.equals("Login")});
        password = takePasswordFromClient(password);
        userRole = takeRoleFromClient();
        user = new User(login, password, userRole);
        return user;
    }

    protected String takeLoginFromClient(String temporaryLogin, boolean[] setOfConditions) {
        boolean userDataCorrect = true;
        while (temporaryLogin.isEmpty() || (setOfConditions[0] && userDataCorrect) || (setOfConditions[1] && !userDataCorrect)) {
            String enterPrompt = setProperEnterPrompt(setOfConditions[0]);
            sendMessageWithIOCommunication(enterPrompt);
            temporaryLogin = takeMessageWithIOCommunication();
            String finalTemporaryLogin = temporaryLogin;
            userDataCorrect = checkIfDataCorrect(users, user -> user.login().equals(finalTemporaryLogin));
            String errorPrompt = setProperErrorPrompt(userDataCorrect);
            sendProperErrorPrompt(errorPrompt, setOfConditions, userDataCorrect);
        }
        return temporaryLogin;
    }

    private void sendProperErrorPrompt(String errorPrompt, boolean[] setOfConditions, boolean userDataCorrect) {
        if (setOfConditions[0] && userDataCorrect) {
            sendMessageWithIOCommunication(errorPrompt);
        } else if (setOfConditions[1] && !userDataCorrect) {
            sendMessageWithIOCommunication(errorPrompt);
        }
    }

    protected String takePasswordFromClient(String temporaryPassword) {
        while (temporaryPassword.isEmpty()) {
            sendMessageWithIOCommunication(getPleaseEnterYourPassword());
            temporaryPassword = takeMessageWithIOCommunication();
        }
        return temporaryPassword;
    }

    protected UserRole takeRoleFromClient() {
        UserRole temporaryUserRole = null;
        sendMessageWithIOCommunication(getPleaseEnterYourRole());
        while (temporaryUserRole == null) {
            String roleString = takeMessageWithIOCommunication();
            roleString = roleString.toLowerCase();
            switch (roleString) {
                case "admin" -> temporaryUserRole = UserRole.ADMIN;
                case "user" -> temporaryUserRole = UserRole.USER;
                default -> sendMessageWithIOCommunication(getDidntRecognizeThisRole());
            }
        }
        return temporaryUserRole;
    }

    protected <T> boolean checkIfDataCorrect(List<T> list, Predicate<T> predicate) {
        boolean dataCorrect = false;
        for (T iterator : list) {
            dataCorrect = predicate.test(iterator);
            if (dataCorrect) {
                break;
            }
        }
        return dataCorrect;
    }

    protected void takeDataFromUserData(Consumer<User> userOperation, String login) {
        try {
            for (User user : users) {
                if (user.login().equals(login)) {
                    userOperation.accept(user);
                    break;
                }
            }
        } catch (NullPointerException nullPointerException) {
            System.err.printf("%s: There is no such login so I couldn't find proper account", CLASS_NAME);
        }
    }

    protected void addNewUser() {
        users.add(user);
        saveAllDataToTable(users, USERS_TABLE, User.class);
    }

    protected <T> void saveAllDataToTable(List<T> list, String tableName, Class<T> valueClass) {
        List<T> recordsInTable = loadTableToList(tableName, valueClass);
        if (!recordsInTable.isEmpty() && (list.size() < recordsInTable.size() && User.class.equals(valueClass))) {
            List<T> toDelete = recordsInTable.stream().filter(record -> !list.contains(record)).toList();
            deleteUserFromUsersTable((User) toDelete.get(0));
        } else {
            for (T element : list) {
                if (recordsInTable.isEmpty() || list.indexOf(element) > recordsInTable.size() - 1) {
                    insertDataIntoTable(element, valueClass);
                } else {
                    T record = recordsInTable.get(list.indexOf(element));
                    if (element.hashCode() != record.hashCode()) {
                        replaceDataInTable(element, record, valueClass);
                    }
                }
            }
        }
        sendMessageWithIOCommunication(getAllSavedProperly());
    }

    private <T> void insertDataIntoTable(T element, Class<T> valueClass) {
        if (valueClass.equals(User.class)) {
            insertUserIntoUsersTable((User) element);
        } else if (valueClass.equals(Mail.class)) {
            insertMailIntoMailsTable((Mail) element);
        }
    }

    private void insertUserIntoUsersTable(User user) {
        JOOQ.insertInto(
                        table("users"),
                        field("login"),
                        field("password"),
                        field("user_role"))
                .values(
                        user.login(),
                        user.password(),
                        user.userRole().toString())
                .execute();
    }

    private void insertMailIntoMailsTable(Mail mail) {
        JOOQ.insertInto(
                        table("mails"),
                        field("sender"),
                        field("receiver"),
                        field("date"),
                        field("mail_status"),
                        field("subject"),
                        field("mail"))
                .values(
                        mail.getSender(),
                        mail.getReceiver(),
                        mail.getDate(),
                        mail.getMailStatus().toString(),
                        mail.getSubject(),
                        mail.getMail())
                .execute();
    }

    private void deleteUserFromUsersTable(User userRecord) {
        Record record = JOOQ.select(asterisk()).from(table("users")).where(field("login").eq(userRecord.login())).fetchOne();
        assert record != null;
        int userId = record.get(field("user_id", Integer.class));
        JOOQ.deleteFrom(table("users")).where(field("user_id").eq(userId)).execute();
    }

    private <T> void replaceDataInTable(T element, T record, Class<T> valueClass) {
        if (valueClass.equals(User.class)) {
            replaceUserInUsersTable((User) element, (User) record);
        } else if (valueClass.equals(Mail.class)) {
            replaceMailInMailsTable((Mail) element, (Mail) record);
        }
    }

    private void replaceUserInUsersTable(User user, User userRecord) {
        Record record = JOOQ.select(asterisk()).from(table("users")).where(field("login").eq(userRecord.login())).fetchOne();
        assert record != null;
        int userId = record.get(field("user_id", Integer.class));
        JOOQ.update(table("users"))
                .set(field("login"), user.login())
                .set(field("password"), user.password())
                .set(field("user_role"), user.userRole().toString())
                .where(field("user_id").eq(userId))
                .execute();
    }

    private void replaceMailInMailsTable(Mail mail, Mail mailRecord) {
        Record record = JOOQ.select(asterisk()).from(table("mails")).where(field("date").eq(mailRecord.getDate())).fetchOne();
        assert record != null;
        int mailId = record.get(field("mail_id", Integer.class));
        JOOQ.update(table("mails"))
                .set(field("sender"), mail.getSender())
                .set(field("receiver"), mail.getReceiver())
                .set(field("date"), mail.getDate())
                .set(field("mail_status"), mail.getMailStatus().toString())
                .set(field("subject"), mail.getSubject())
                .set(field("mail"), mail.getMail())
                .where(field("mail_id").eq(mailId))
                .execute();
    }

    protected void sendMessageWithIOCommunication(String message) {
        COMMUNICATION.sendMessage(message);
    }

    protected String takeMessageWithIOCommunication() {
        return COMMUNICATION.takeMessage();
    }

    protected String getDidntRecognizeThisChoice() {
        return "I didn't recognize this choice";
    }


    private String setProperEnterPrompt(boolean conditionOne) {
        String enterPrompt;
        if ("MailboxSettings".equals(CLASS_NAME)) {
            enterPrompt = conditionOne ? "Please enter new login:\n" : "Please enter users login:\n";
        } else {
            enterPrompt = "Please enter your login:\n";
        }
        return enterPrompt;
    }

    private String setProperErrorPrompt(boolean userDataCorrect) {
        return userDataCorrect ? String.format("%s: This name is taken", CLASS_NAME) : String.format("%s: Login unrecognized", CLASS_NAME);
    }

    private String getPleaseEnterYourPassword() {
        return "Please enter your password:\n";
    }

    private String getPleaseEnterYourRole() {
        return "Please enter your role (admin/user):\n";
    }

    private String getDidntRecognizeThisRole() {
        return String.format("%s: I didn't recognize this role, please try again (admin/user)\n", CLASS_NAME);
    }

    private String getAllSavedProperly() {
        return String.format("%s: All saved properly", CLASS_NAME);
    }
}
