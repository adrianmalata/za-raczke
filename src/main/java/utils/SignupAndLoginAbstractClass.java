package utils;

import database.Database;

public abstract class SignupAndLoginAbstractClass extends TakeDataFromClient {

    public SignupAndLoginAbstractClass(IOCommunication communication, Database database) {
        super(communication, database);
    }

    public void start() {
        takeInputFromClientInWelcomeScreen();
    }

    protected abstract void takeInputFromClientInWelcomeScreen();

    protected String takeUsersLoginFromClient(String temporaryLogin) {
        boolean[] setOfConditions = new boolean [] {CLASS_NAME.equals("Signup"), CLASS_NAME.equals("Login")};
        return takeLoginFromClient(temporaryLogin, setOfConditions);
    }

    protected String getWelcomeScreen() {
        return String.format("This is %s screen", getClass().getSimpleName().toLowerCase());
    }

    protected String getThankYouForSignup() {
        return String.format("Thank you for your signup %s!", user.login());
    }

    protected String getClosingScreen() {
        return String.format("%s: Going back to home screen...", CLASS_NAME);
    }

    protected String getCommunicate(String subject) {
        return String.format("%s: %s is incorrect, please write it again\n", CLASS_NAME, subject);
    }
}