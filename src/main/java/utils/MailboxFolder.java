package utils;


import database.Database;
import java.util.ArrayList;
import java.util.List;

public abstract class MailboxFolder extends TakeDataFromClient {

    protected final String CLASS_NAME = getClass().getSimpleName();
    protected final User USER;
    protected boolean exitFlag;

    public MailboxFolder(IOCommunication communication, Database database, User user) {
        super(communication, database);
        this.USER = user;
    }

    public void start() {
        while (!exitFlag) {
            mails = loadTableToList(MAILS_TABLE, Mail.class);
            List<Mail> mailsDedicated = collectMails(false);
            showMails();
            setChoice(mailsDedicated);
        }
        saveAllDataToTable(mails, MAILS_TABLE, Mail.class);
    }

    protected void setChoice(List<Mail> mailsDedicated) {
        showSetChoiceHeader();
        char choice = String.valueOf(takeMessageWithIOCommunication()).charAt(0);
        handleChoiceInputInMailboxFolder(choice, mailsDedicated);
    }

    private void showSetChoiceHeader() {
        sendMessageWithIOCommunication(getSetChoiceHeader());
    }

    private void handleChoiceInputInMailboxFolder(char choice, List<Mail> mailsDedicated) {
        if (Character.isDigit(choice)) {
            boolean mailExists = showMail(mailsDedicated, Character.getNumericValue(choice) - 1);
            if (mailExists) {
                showProperSetChoiceFooter();
                String mailChoice = takeMessageWithIOCommunication();
                markAsRead(mailChoice, mailsDedicated, choice);
            }
        } else {
            exitFlag = true;
        }
    }

    protected boolean showMail(List<Mail> mailsToShow, int mailPosition) {
        try {
            Mail mailToShow = mailsToShow.get(mailPosition);
            sendMessageWithIOCommunication(getMailDetailInformation(mailToShow));
            return true;
        } catch (RuntimeException runtimeException) {
            sendMessageWithIOCommunication(getYouCannotRequestForThatPosition());
            return false;
        }
    }

    private void showProperSetChoiceFooter() {
        if ("Unread".equals(CLASS_NAME)) {
            sendMessageWithIOCommunication(getTypeRToMarkAsRead());
        } else {
            sendMessageWithIOCommunication(getTypeBToGoBack());
        }
    }

    private void markAsRead(String mailChoice, List<Mail> mailsDedicated, char choice) {
        if ("r".equals(mailChoice)) {
            mailsDedicated.get(Character.getNumericValue(choice) - 1).setMailStatus(MailStatus.READ);
            sendMessageWithIOCommunication(getMailMarkedAsRead());
            exitFlag = true;
        }
    }

    protected void showMails() {
        sendMessageWithIOCommunication(getTheseAreYourFolderMails());
        List<String> mailsToShow = collectMails(true);
        for (String mail : mailsToShow) {
            sendMessageWithIOCommunication(mail);
        }
    }

    protected <T> List<T> collectMails(boolean asString) {
        List<T> mailsToShow = new ArrayList<>();
        for (Mail mail : mails) {
            MailStatus mailStatus = mail.getMailStatus();
            if ("Unread".equals(CLASS_NAME) && USER.login().equals(mail.getReceiver()) && mailStatus.equals(MailStatus.UNREAD)) {
                formatMailsProperly(mailsToShow, mail, asString);
                if (mailsToShow.size() >= 5) {
                    break;
                }
            } else if ("Read".equals(CLASS_NAME) && USER.login().equals(mail.getReceiver()) && mailStatus.equals(MailStatus.READ)) {
                formatMailsProperly(mailsToShow, mail, asString);
            } else if ("Sent".equals(CLASS_NAME) && USER.login().equals(mail.getSender())) {
                formatMailsProperly(mailsToShow, mail, asString);
            }
        }
        return mailsToShow;
    }

    @SuppressWarnings("unchecked")
    private <T> void formatMailsProperly(List<T> mailsToShow, Mail mail, boolean asString) {
        if (asString) {
            if ("Sent".equals(CLASS_NAME)) {
                mailsToShow.add((T) String.format("Date: %s\tTo: %s\tSubject: %s", mail.getDate(), mail.getReceiver(), mail.getSubject()));
            } else {
                mailsToShow.add((T) String.format("Date: %s\tFrom: %s\tSubject: %s", mail.getDate(), mail.getSender(), mail.getSubject()));
            }
        } else {
            mailsToShow.add((T) mail);
        }
    }

    private String getSetChoiceHeader() {
        return """
                ------------------------------------------------
                Type the following number of mail to open it, or type "b" to go back to Mailbox page
                ------------------------------------------------
                """;
    }

    private String getMailDetailInformation(Mail mailToShow) {
        return String.format(
                """
                ------------------------------------------------
                Date: %s
                From: %s
                To: %s
                Subject: %s
                Body:
                %s
                ------------------------------------------------""",
                mailToShow.getDate(), mailToShow.getSender(),
                mailToShow.getReceiver(), mailToShow.getSubject(), mailToShow.getMail()
        );
    }

    private String getYouCannotRequestForThatPosition() {
        return String.format("%s: You cannot request for that position", CLASS_NAME);
    }

    private String getTypeRToMarkAsRead() {
        return "Type \"r\" to mark as READ, type \"b\" to go back to Mailbox page\n";
    }

    private String getTypeBToGoBack() {
        return "Type \"b\" to go back to Mailbox page\n";
    }

    private String getMailMarkedAsRead() {
        return "Mail marked as READ and moved to READ folder";
    }

    private String getTheseAreYourFolderMails() {
        return String.format(
                """
                ------------------------------------------------
                These are your %s mails %s
                ------------------------------------------------""", CLASS_NAME.toLowerCase(), USER.login()
        );
    }
}
