package utils;

public enum MailStatus {
    UNREAD,
    READ
}
