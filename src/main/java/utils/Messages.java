package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.time.Instant;
import java.util.Properties;

public class Messages {

    private final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private final Properties PROPERTIES = new PropertiesUtils().loadPropertiesFromFile();
    private final String SERVER_VERSION = PROPERTIES.getProperty("SERVER_VERSION");
    private final String SERVER_CREATION_DATE = PROPERTIES.getProperty("SERVER_CREATION_DATE");

    public Messages() {
    }

    public String getHelpMessageJson() {
        ObjectNode helpMessageJson = OBJECT_MAPPER.createObjectNode();
        helpMessageJson.put("help", "Shows this help message");
        helpMessageJson.put("info", "Returns server version number and creation date");
        helpMessageJson.put("uptime", "Returns the server lifetime");
        helpMessageJson.put("stop", "Stops the server and the client simultaneously");
        return helpMessageJson + "\n ";
    }

    public String getInfoMessageJson() {
        ObjectNode infoMessageJson = OBJECT_MAPPER.createObjectNode();
        infoMessageJson.put("serverVersion", String.valueOf(SERVER_VERSION));
        infoMessageJson.put("serverCreationDate", String.valueOf(SERVER_CREATION_DATE));
        return infoMessageJson + "\n ";
    }

    public String getUptimeMessageJson(Instant serverStartInstant) {
        ObjectNode uptimeMessageJson = OBJECT_MAPPER.createObjectNode();
        uptimeMessageJson.put("uptime", String.valueOf(serverStartInstant));
        return uptimeMessageJson + "\n ";
    }

    public String getDefaultMessageString() {
        return "Undefined command\n ";
    }
}