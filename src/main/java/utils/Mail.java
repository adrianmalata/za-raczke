package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Objects;

public class Mail {

    private String sender;
    private String receiver;
    private String date;
    private MailStatus mailStatus;
    private String subject;
    private String mail;

    public Mail() {}

    public Mail(User sender) {
        this.sender = sender.login();
        this.mailStatus = MailStatus.UNREAD;
    }

    public Mail(User sender, User receiver, LocalDateTime date, String subject, String mail) {
        this.sender = sender.login();
        this.receiver = receiver.login();
        this.date = date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
        this.mailStatus = MailStatus.UNREAD;
        this.subject = subject;
        this.mail = mail;
    }

    public Mail(String sender, String receiver, String date, MailStatus mailStatus, String subject, String mail) {
        this.sender = sender;
        this.receiver = receiver;
        this.date = date;
        this.mailStatus = mailStatus;
        this.subject = subject;
        this.mail = mail;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public MailStatus getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(MailStatus mailStatus) {
        this.mailStatus = mailStatus;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, receiver, date, mailStatus, subject, mail);
    }
}
