package utils;

import org.jooq.Record;
import org.jooq.RecordMapper;

public class UserAndMailRecordMapper<T> implements RecordMapper<Record, T> {

    private final Class<T> VALUE_CLASS;

    public UserAndMailRecordMapper(Class<T> valueClass) {
        this.VALUE_CLASS = valueClass;
    }

    @Override
    public T map(Record record) {
        if (VALUE_CLASS.equals(User.class)) {
            return VALUE_CLASS.cast(
                    new User(
                            record.get("login", String.class),
                            record.get("password", String.class),
                            record.get("user_role", UserRole.class)
                    )
            );
        } else if (VALUE_CLASS.equals(Mail.class)) {
            return VALUE_CLASS.cast(
                    new Mail(
                            record.get("sender", String.class),
                            record.get("receiver", String.class),
                            record.get("date", String.class),
                            record.get("mail_status", MailStatus.class),
                            record.get("subject", String.class),
                            record.get("mail", String.class)
                    )
            );
        } else {
            return null;
        }
    }
}
