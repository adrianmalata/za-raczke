package login;

import database.Database;
import mailbox.Mailbox;
import utils.IOCommunication;
import utils.SignupAndLoginAbstractClass;
import utils.User;

import static org.jooq.impl.DSL.table;

public class Login extends SignupAndLoginAbstractClass {

    private User properUserData;

    public Login(IOCommunication communication, Database database) {
        super(communication, database);
    }

    @Override
    public void start() {
        if (JOOQ.fetch(table(USERS_TABLE)).isEmpty()) {
            sendMessageWithIOCommunication(getClosingScreen());
        } else {
            super.start();
        }
    }

    @Override
    protected void takeInputFromClientInWelcomeScreen() {
        sendMessageWithIOCommunication(getWelcomeScreen());
        makeUserFromClientInput();
        checkIfProvidedDataIsCorrect();
    }

    @Override
    public User makeUserFromClientInput() {
        login = takeUsersLoginFromClient(login);
        password = takePasswordFromClient(password);
        takeDataFromUserData(user -> userRole = user.userRole(), login);
        user = new User(login, password, userRole);
        return user;
    }

    private void checkIfProvidedDataIsCorrect() {
        boolean nameExist = checkIfDataCorrect(users, user -> user.login().equals(login));
        if (nameExist) {
            takeDataFromUserData(user -> properUserData = new User(user.login(), user.password(), user.userRole()), login);
            boolean passwordCorrect;
            do {
                passwordCorrect = checkIfDataCorrect(users, user -> properUserData.password().equals(password));
                if (passwordCorrect) {
                    acceptUserLogin();
                    break;
                } else {
                    sendMessageWithIOCommunication(getCommunicate("Password"));
                }
                password = takeMessageWithIOCommunication();
            } while (true);
        } else {
            sendMessageWithIOCommunication(getCommunicate("Login"));
        }
    }

    protected void acceptUserLogin() {
        sendMessageWithIOCommunication(getSuccessfullyLogged());
        new Mailbox(COMMUNICATION, DATABASE, user).start();
    }

    private String getSuccessfullyLogged() {
        return "Login: Successfully logged.";
    }
}