package client;

import utils.PropertiesUtils;

import java.util.Properties;

public class StartClient {
    public static void main(String[] args) {
        Properties properties = new PropertiesUtils().loadPropertiesFromFile();
        String ipAddress = properties.getProperty("CLIENT_IP");
        int port = Integer.parseInt(properties.getProperty("PORT"));

        Client client = new Client();

        client.connectToServer(ipAddress, port);
        client.handleCommunication();
    }
}
