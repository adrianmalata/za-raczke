package client;

import utils.IOCommunication;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

    private final String CLASS_NAME = getClass().getSimpleName();
    private final Scanner SCANNER = new Scanner(System.in);
    protected IOCommunication communication;
    protected Socket clientSocket;
    private boolean stopClient;

    public void connectToServer(String ipAddress, int port) {
        createClientSocket(ipAddress, port);
        openCommunication();
        System.out.printf("%s: Connection established\n", CLASS_NAME);
    }

    private void createClientSocket(String ipAddress, int port) {
        try {
            clientSocket = new Socket(ipAddress, port);
        } catch (UnknownHostException unknownHostException) {
            System.err.printf("%s: The IP address of the host could not be determined\n", CLASS_NAME);
        } catch (IOException ioException) {
            System.err.printf("%s: I/O error occurred while creating the socket\n", CLASS_NAME);
        } catch (IllegalArgumentException illegalArgumentException) {
            System.err.printf("%s: The port parameter is outside the specified range of valid port values, which is between 0 and 65535\n", CLASS_NAME);
        }
    }

    private void openCommunication() {
        communication = new IOCommunication(clientSocket);
    }

    public void handleCommunication() {
        while (!stopClient) {
            System.out.println("\n" + receiveMessages());
            sendMessages();
        }
    }

    protected StringBuilder receiveMessages() {
        String response;
        StringBuilder stringBuilder = new StringBuilder();
        while (true) {
            response = communication.takeMessage();
            if (response.isEmpty()) {
                break;
            }
            stringBuilder.append("\n").append(response);
        }
        return stringBuilder.delete(0,1);
    }

    protected void sendMessages() {
        String command = SCANNER.nextLine();
        communication.sendMessage(command);
        command = command.toLowerCase();
        if ("stop".equals(command)) {
            stopClient();
            SCANNER.close();
            stopClient = true;
        }
    }

    protected void stopClient() {
        System.out.printf(receiveMessages().toString());
        closeClientSocket();
    }

    private void closeClientSocket() {
        try {
            communication.closeCommunication();
            clientSocket.close();
        } catch (IOException ioException) {
            System.err.printf("%s: I/O error occurred while closing the client socket\n", CLASS_NAME);
        }
    }
}
